FROM registry.hub.docker.com/library/golang:1.20 as builder
RUN adduser --system appuser

WORKDIR $GOPATH/src/gitlab.switch.ch/memoriav/memobase-2020/services/indexer
COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/app -a gitlab.switch.ch/memoriav/memobase-2020/services/indexer/cmd/identify

FROM registry.hub.docker.com/library/perl:5.30-slim-buster
WORKDIR /app
COPY --from=builder /go/src/gitlab.switch.ch/memoriav/memobase-2020/services/indexer/bin/app /app
COPY --from=builder /etc/passwd /etc/passwd

RUN apt-get update && \
apt-get install -y exiftool gnupg software-properties-common && \
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 20F802FE798E6857 && \
add-apt-repository "deb https://www.itforarchivists.com/ buster main" && \
apt-get update && \
apt-get install -y siegfried ffmpeg imagemagick ca-certificates && \
apt-get purge -y gnupg software-properties-common && \
apt-get autoremove -y && \
apt-get clean
RUN roy build && sf -update

USER appuser

ADD web/static/ /app/static
ADD web/template /app/web/template
# ADD ./start.sh /app/start.sh

EXPOSE 8082

ENTRYPOINT ["/app/app", "-cfg", "/app/configs/indexer.toml"]
